﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace RocketCore.Engine.Replication
{
    static class Client
    {
        private static TcpClient client;
        private static volatile bool thread_running = false;

        internal static void Start()
        {
            try
            {
                client = new TcpClient("127.0.0.1", 1360); //подключение к Slave-ядру !TODO убрать хардкод

                Console.WriteLine("SLAVE CORE: connection established");

                if (!thread_running)
                {
                    Thread thread = new Thread(new ThreadStart(ReplicationThread));
                    thread.Start();
                    thread_running = true;

                    Console.WriteLine("SLAVE CORE: replication thread started");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("==TCP ERROR==");
                Console.WriteLine(e.ToString());
            }
        }

        private static void ReplicationThread()
        {
            //логика выгребания из очереди в сокет Slave-ядра
            while (true)
            {
                //попытка отправить реплицируемые данные в Slave-ядро
                FuncCallReplica fc_replica;
                if (Queues.slave_queue.TryPeek(out fc_replica))
                {
                    if (fc_replica.str_args != null) //реплицируем FC
                    {
                        bool _sent = SocketIO.Write(client, fc_replica.Serialize());

                        if (_sent)
                        {
                            Console.WriteLine("SLAVE CORE: message sent");
                            Queues.slave_queue.TryDequeue(out fc_replica);
                        }
                        else
                        {
                            Console.WriteLine("SLAVE CORE: failed to send a message (dc)");
                            break;
                        }
                    }
                    else //выполняем операции со снэпшотом
                    {
                        bool _success = false;

                        Flags.backup_restore_in_proc = true; //установка флага отклонения вызовов всех функций                        

                        if (fc_replica.func_id == (int)FuncIds.BackupMasterSnapshot) //выполняем backup Master-ядра
                        {
                            _success = Snapshot.BackupMasterSnapshotInner(client);
                        }
                        else if (fc_replica.func_id == (int)FuncIds.RestoreMasterSnapshot) //выполняем restore Master-ядра
                        {
                            _success = Snapshot.RestoreSnapshotInner(client, false);
                        }
                        else if (fc_replica.func_id == (int)FuncIds.RestoreSlaveSnapshot) //выполняем restore Slave-ядра
                        {
                            _success = Snapshot.RestoreSnapshotInner(client, true);
                        }

                        Flags.backup_restore_in_proc = false; //сброс флага отклонения вызовов всех функций

                        if (_success) Queues.slave_queue.TryDequeue(out fc_replica);
                        else break;
                    }
                }
            }

            if (client != null) client.Close();
            Console.WriteLine("SLAVE CORE: connection closed by Slave-core");
            Console.WriteLine("SLAVE CORE: exiting replication thread");
            thread_running = false;
        }

        internal static void Stop()
        {
            if (client != null)
            {
                client.Close();
                Console.WriteLine("SLAVE CORE: connection closed by Master-core");
            }
        }

    }
}
